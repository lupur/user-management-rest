package com.lupur.api.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lupur.business.dto.RoleDTO;
import com.lupur.business.dto.UserDTO;
import com.lupur.business.service.UserService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Created by Razvan Alexandru Lupu on 20-Jul-17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(value = UserController.class, secure = false)
public class UserControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;


    @Before
    public void setUp() throws Exception {

    }


    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getUsers_success() throws Exception {

        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        UserDTO userDTO2 = new UserDTO(2, "John", "Branch", "johnbranch", "password", "johnbranch@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        List<UserDTO> userDTOList = new ArrayList<>();
        userDTOList.add(userDTO);
        userDTOList.add(userDTO2);
        when(userService.getUsers()).thenReturn(userDTOList);

        mockMvc.perform(get("/users/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(content().json(asJsonString(userDTOList)));
        verify(userService, times(1)).getUsers();


    }

    @Test
    public void getUser_success() throws Exception {

        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.getUser(userDTO.getId())).thenReturn(userDTO);

        mockMvc.perform(get("/users/" + userDTO.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(asJsonString(userDTO)));
        verify(userService, times(1)).getUser(userDTO.getId());


    }

    @Test
    public void getUser_notFound() throws Exception {

        Integer id = 1;
        when(userService.getUser(id)).thenThrow(new IllegalArgumentException());

        mockMvc.perform(get("/users/" + id))
                .andExpect(status().isNotFound());
        verify(userService, times(1)).getUser(id);


    }


    @Test
    public void createUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.create(userDTO)).thenReturn(userDTO);


        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(userDTO)))
                .andExpect(status().isCreated())
                .andExpect(content().json(asJsonString(userDTO)));
        verify(userService, times(1)).create(Matchers.any(UserDTO.class));

    }

    @Test
    public void createUser_validateError() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "l", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.create(userDTO)).thenReturn(userDTO);


        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(userDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", containsString("BAD_REQUEST")));
        verify(userService, times(0)).create(userDTO);

    }

    @Test(expected = NestedServletException.class)
    public void createUser_duplicateUsername() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.create(userDTO)).thenThrow(new DataIntegrityViolationException(""));


        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(userDTO)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", containsString("INTERNAL_SERVER_ERROR")));
        verify(userService, times(1)).create(Matchers.any(UserDTO.class));

    }

    @Test
    public void deleteUser_success() throws Exception {

        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        when(userService.deleteUser(userDTO.getId())).thenReturn(userDTO);
        when(userService.getUser(userDTO.getId())).thenReturn(userDTO);
        mockMvc.perform(delete("/users/" + userDTO.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(asJsonString(userDTO)));
        verify(userService, times(1)).deleteUser(userDTO.getId());
    }

    @Test
    public void deleteUser_notFound() throws Exception {

        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());

        doThrow(new IllegalArgumentException()).when(userService).deleteUser(userDTO.getId());
        when(userService.getUser(userDTO.getId())).thenReturn(userDTO);
        mockMvc.perform(delete("/users/" + userDTO.getId()))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));
        verify(userService, times(1)).deleteUser(userDTO.getId());
    }

    @Test
    public void updateUser_success() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.updateUser(userDTO.getId(), userDTO)).thenReturn(userDTO);


        mockMvc.perform(put("/users/" + userDTO.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(userDTO)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(content().json(asJsonString(userDTO)));
        verify(userService, times(1)).updateUser(Matchers.anyInt(), Matchers.any(UserDTO.class));
    }

    @Test
    public void updateUser_notFound() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.updateUser(userDTO.getId(), userDTO)).thenThrow(new IllegalArgumentException());


        mockMvc.perform(put("/users/" + userDTO.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(userDTO)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isNotFound());
        verify(userService, times(1)).updateUser(Matchers.anyInt(), Matchers.any(UserDTO.class));
    }


    @Test
    public void updateUser_validationError() throws Exception {
        UserDTO userDTO = new UserDTO(1, "Lupu", "Razvan", "l", "password", "lupurazvan@gmail.com", new SimpleDateFormat("yyyy-MM-dd").parse("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<RoleDTO>());
        when(userService.updateUser(userDTO.getId(), userDTO)).thenThrow(new IllegalArgumentException());


        mockMvc.perform(put("/users/" + userDTO.getId())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(asJsonString(userDTO)))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isBadRequest());
        verify(userService, times(0)).updateUser(Matchers.anyInt(), Matchers.any(UserDTO.class));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}