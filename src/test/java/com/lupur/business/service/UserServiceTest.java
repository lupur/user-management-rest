package com.lupur.business.service;

import com.lupur.business.dto.UserDTO;
import com.lupur.business.mapper.UserMapper;
import com.lupur.data.entity.Role;
import com.lupur.data.entity.User;
import com.lupur.data.repository.UserJpaRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityNotFoundException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Razvan Alexandru Lupu on 21-Jul-17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ModelMapper.class})
public class UserServiceTest {


    @InjectMocks
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @Mock
    private UserJpaRepository userJpaRepository;

    @Mock
    private UserMapper mockUserMapper;

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void create_success() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        when(userJpaRepository.save(user)).thenReturn(user);
        when(mockUserMapper.getDTO(user)).thenReturn(userDTO);
        when(mockUserMapper.getEntity(userDTO)).thenReturn(user);

        assertEquals(userDTO, userService.create(userDTO));

        verify(userJpaRepository, times(1)).save(user);
        verify(mockUserMapper, times(1)).getDTO(user);
        verify(mockUserMapper, times(1)).getEntity(userDTO);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void create_duplicateUsername() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        when(userJpaRepository.save(user)).thenThrow(new DataIntegrityViolationException(""));
        when(mockUserMapper.getDTO(user)).thenReturn(userDTO);
        when(mockUserMapper.getEntity(userDTO)).thenReturn(user);

        userService.create(userDTO);

        verify(userJpaRepository, times(1)).save(user);
        verify(mockUserMapper, times(0)).getDTO(user);
        verify(mockUserMapper, times(1)).getEntity(userDTO);
    }

    @Test
    public void getUsers() throws Exception {

        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        User user2 = new User(2, "John", "Branch", "johnbranch", "password", "johnbranch@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        List<User> userList = new ArrayList<>();
        List<UserDTO> userDTOList = new ArrayList<>();
        userList.add(user);
        userList.add(user2);
        for (User u : userList) {
            userDTOList.add(modelMapper.map(u, UserDTO.class));
        }

        when(userJpaRepository.findAll()).thenReturn(userList);
        when(mockUserMapper.getDTO(user)).thenReturn(modelMapper.map(user, UserDTO.class));
        when(mockUserMapper.getDTO(user2)).thenReturn(modelMapper.map(user2, UserDTO.class));

        assertEquals(userDTOList, userService.getUsers());

        verify(userJpaRepository, times(1)).findAll();
        verify(mockUserMapper, times(2)).getDTO(Matchers.any(User.class));
    }

    @Test
    public void getUser_success() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());

        when(userJpaRepository.findOne(user.getId())).thenReturn(user);
        when(mockUserMapper.getDTO(user)).thenReturn(modelMapper.map(user, UserDTO.class));

        assertEquals(modelMapper.map(user, UserDTO.class), userService.getUser(user.getId()));
        verify(userJpaRepository, times(1)).findOne(user.getId());
        verify(mockUserMapper, times(1)).getDTO(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void getUser_notFound() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());

        when(userJpaRepository.findOne(user.getId())).thenReturn(null);
        userService.getUser(user.getId());
        verify(userJpaRepository, times(1)).findOne(user.getId());
        verify(mockUserMapper, times(0)).getDTO(user);
    }

    @Test
    public void deleteUser_success() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        when(userJpaRepository.findOne(user.getId())).thenReturn(user);
        doAnswer(invocationOnMock -> null).when(userJpaRepository).delete(user);
        when(mockUserMapper.getDTO(user)).thenReturn(userDTO);
        when(mockUserMapper.getEntity(userDTO)).thenReturn(user);

        assertEquals(userDTO, userService.deleteUser(userDTO.getId()));

        verify(userJpaRepository, times(1)).findOne(user.getId());
        verify(userJpaRepository, times(1)).delete(user.getId());
        verify(mockUserMapper, times(1)).getDTO(user);
        verify(mockUserMapper, times(0)).getEntity(userDTO);
    }

    @Test(expected = IllegalArgumentException.class)
    public void deleteUser_notFound() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        when(userJpaRepository.findOne(user.getId())).thenThrow(new IllegalArgumentException());
        doAnswer(invocationOnMock -> null).when(userJpaRepository).delete(user);
        when(mockUserMapper.getDTO(user)).thenReturn(userDTO);
        when(mockUserMapper.getEntity(userDTO)).thenReturn(user);

        userService.deleteUser(userDTO.getId());

        verify(userJpaRepository, times(1)).findOne(user.getId());
        verify(userJpaRepository, times(0)).delete(user.getId());
        verify(mockUserMapper, times(1)).getDTO(user);
        verify(mockUserMapper, times(0)).getEntity(userDTO);
    }

    @Test
    public void updateUser_success() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        when(userJpaRepository.save(user)).thenReturn(user);
        when(userJpaRepository.findOne(user.getId())).thenReturn(user);
        when(mockUserMapper.getDTO(user)).thenReturn(userDTO);
        when(mockUserMapper.getEntity(userDTO)).thenReturn(user);

        assertEquals(userDTO, userService.updateUser(userDTO.getId(), userDTO));

        verify(userJpaRepository, times(1)).save(user);
        verify(mockUserMapper, times(1)).getDTO(user);
        verify(mockUserMapper, times(1)).getEntity(userDTO);
    }

    @Test(expected = EntityNotFoundException.class)
    public void updateUser_failure() throws Exception {
        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        UserDTO userDTO = modelMapper.map(user, UserDTO.class);

        when(userJpaRepository.findOne(user.getId())).thenReturn(null);

        when(mockUserMapper.getDTO(user)).thenReturn(userDTO);
        when(mockUserMapper.getEntity(userDTO)).thenReturn(user);

        userService.updateUser(userDTO.getId(), userDTO);

        verify(userJpaRepository, times(1)).save(user);
        verify(mockUserMapper, times(1)).getDTO(user);
        verify(mockUserMapper, times(1)).getEntity(userDTO);
    }

    @Test
    public void findByUsername_success() throws Exception {

        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        when(userJpaRepository.findByUsername(user.getUsername())).thenReturn(user);
        when(mockUserMapper.getDTO(user)).thenReturn(modelMapper.map(user, UserDTO.class));

        assertEquals(modelMapper.map(user, UserDTO.class), userService.findByUsername(user.getUsername()));

        verify(userJpaRepository, times(1)).findByUsername(user.getUsername());
        verify(mockUserMapper, times(1)).getDTO(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByUsername_notFound() throws Exception {

        User user = new User(1, "Lupu", "Razvan", "lupurazvan", "password", "lupurazvan@gmail.com", Date.valueOf("1995-07-15"), "0728098896", "Cluj-Napoca", new HashSet<Role>());
        when(userJpaRepository.findByUsername(user.getUsername())).thenThrow(new IllegalArgumentException());
        when(mockUserMapper.getDTO(user)).thenReturn(modelMapper.map(user, UserDTO.class));

        assertEquals(modelMapper.map(user, UserDTO.class), userService.findByUsername(user.getUsername()));

        verify(userJpaRepository, times(1)).findByUsername(user.getUsername());
        verify(mockUserMapper, times(1)).getDTO(user);
    }

}