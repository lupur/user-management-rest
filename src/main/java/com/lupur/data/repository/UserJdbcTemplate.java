package com.lupur.data.repository;

import com.lupur.data.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Razvan Alexandru Lupu on 24-Jul-17.
 */

@Repository
public class UserJdbcTemplate {

    public static final String FIND_USERS_BY_ROLE_QUERY = "SELECT U.*, R.ID_ROLE, R.ROLE_NAME FROM USERS AS U INNER JOIN USER_ROLES AS UR ON U.ID_USER=UR.ID_USER INNER JOIN ROLES AS R ON UR.ID_ROLE=R.ID_ROLE WHERE R.ROLE_NAME=:roleName";
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private UserJdbcMapper userJdbcMapper;

    public List<User> findByRole(String roleName) {
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource("roleName", roleName);
        return (List) namedParameterJdbcTemplate.query(FIND_USERS_BY_ROLE_QUERY, sqlParameterSource, userJdbcMapper);
    }


}
