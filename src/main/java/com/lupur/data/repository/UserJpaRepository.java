package com.lupur.data.repository;

import com.lupur.data.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Razvan Alexandru Lupu on 13-Jul-17.
 */

@Repository
public interface UserJpaRepository extends JpaRepository<User,Integer>{

    User findByUsername(String username);

}
