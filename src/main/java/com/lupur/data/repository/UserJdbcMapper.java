package com.lupur.data.repository;

import com.lupur.data.entity.Role;
import com.lupur.data.entity.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Razvan Alexandru Lupu on 24-Jul-17.
 */


public class UserJdbcMapper implements RowMapper {

    @Override
    public Object mapRow(ResultSet resultSet, int i) throws SQLException {
        User user = new User();
        Set<Role> roleSet = new HashSet<>();
        roleSet.add(new Role(resultSet.getInt("id_role"), resultSet.getString("role_name")));
        user.setId(resultSet.getInt("id_user"));
        user.setFirstName(resultSet.getString("first_name"));
        user.setLastName(resultSet.getString("last_name"));
        user.setEmail(resultSet.getString("email"));
        user.setUsername(resultSet.getString("username"));
        user.setPassword(resultSet.getString("password"));
        user.setDateOfBirth(resultSet.getDate("date_of_birth"));
        user.setPhone(resultSet.getString("phone"));
        user.setAddress(resultSet.getString("address"));
        user.setRoles(roleSet);
        return user;
    }
}
