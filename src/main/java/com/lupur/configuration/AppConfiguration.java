package com.lupur.configuration;

import com.lupur.data.repository.UserJdbcMapper;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.HttpStatus;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Razvan Alexandru Lupu on 18-Jul-17.
 */

@Configuration
@EnableSwagger2
@EnableAspectJAutoProxy
@ComponentScan(basePackages = {"com.lupur.api.controller"})
public class AppConfiguration {

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public UserJdbcMapper userJdbcMapper() {
        return new UserJdbcMapper();
    }

    @Bean
    public Docket api() {
        List<ResponseMessage> responseMessageList = new ArrayList<>();
        responseMessageList.add(new ResponseMessageBuilder()
                .code(HttpStatus.NOT_FOUND.value())
                .message("Not found!!!")
                .responseModel(new ModelRef("Error"))
                .build());

        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.lupur.api.controller"))
                .paths(PathSelectors.ant("/users/**"))
                .build()
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo("User management REST",
                "User management REST service description",
                "1.0",
                "http://termsOfServiceUrl.com",
                new Contact("Lupu Razvan Alexandru", "http://www.garmin.com", "Razvan-Alexandru.Lupu@garmin.com"),
                "Apache 2.0",
                "https://www.apache.org/licenses/LICENSE-2.0",
                new ArrayList<>());
    }
}
