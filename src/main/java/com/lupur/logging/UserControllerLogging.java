package com.lupur.logging;

import com.lupur.api.controller.UserController;
import com.lupur.business.dto.UserDTO;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Method;

/**
 * Created by Razvan Alexandru Lupu on 18-Jul-17.
 */

@Aspect
@Component
public class UserControllerLogging {

    private final Logger logger = LoggerFactory.getLogger(UserController.class);

    private static final String REQUEST_MAPPING_PATH = UserController.class.getAnnotation(RequestMapping.class).value()[0];


    @AfterReturning(pointcut = "execution(* com.lupur.api.controller.UserController.getUsers(..))" ,returning = "retVal")
    public void afterReturningAdvice(JoinPoint jp, Object retVal){
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        String path = REQUEST_MAPPING_PATH + method.getAnnotation(GetMapping.class).value()[0];
        logger.info("Method: {}, Operation type: {} ,Path: {}, Parameters: {}, Returning: {}, Returned value: {}",jp.getSignature(),"READ",path,"",retVal.getClass().getSimpleName(),retVal);


    }

    @AfterReturning(pointcut = "execution(* com.lupur.api.controller.UserController.getUser(..)) && args(id)" ,returning = "retVal")
    public void afterReturningAdviceGetUser(JoinPoint jp, Object retVal, Integer id){
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        String path = REQUEST_MAPPING_PATH + method.getAnnotation(GetMapping.class).value()[0];
        Integer resourceId = ((UserDTO) retVal).getId();
        logger.info("Method: {}, Operation type: {} ,Path: {}, Parameters: {}, Resource id: {}, Returning: {}, Returned value: {}",jp.getSignature(),"READ",path,"Id: " + id,resourceId,retVal.getClass().getSimpleName(),retVal);
    }

    @AfterReturning(pointcut = "execution(* com.lupur.api.controller.UserController.updateUser(..)) && args(id,userDTO)" ,returning = "retVal")
    public void afterReturningAdviceUpdateUser(JoinPoint jp, Object retVal, Integer id, UserDTO userDTO){
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        String path = REQUEST_MAPPING_PATH + method.getAnnotation(PutMapping.class).value()[0];
        Integer resourceId = ((UserDTO) retVal).getId();
        logger.info("Method: {}, Operation type: {} ,Path: {}, Parameters: {}, Resource id: {}, Returning: {}, Returned value: {}",jp.getSignature(),"WRITE",path,"Id: " + id + " UserDTO: " + userDTO,resourceId,retVal.getClass().getSimpleName(),retVal);
    }

    @AfterReturning(pointcut = "execution(* com.lupur.api.controller.UserController.createUser(..)) && args(userDTO)" ,returning = "retVal")
    public void afterReturningAdviceCreateUser(JoinPoint jp, Object retVal, UserDTO userDTO){
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        String path = REQUEST_MAPPING_PATH + method.getAnnotation(PostMapping.class).value()[0];
        Integer resourceId = ((UserDTO) retVal).getId();
        logger.info("Method: {}, Operation type: {} ,Path: {}, Parameters: {}, Resource id: {}, Returning: {}, Returned value: {}",jp.getSignature(),"WRITE",path,"UserDTO: " + userDTO,resourceId,retVal.getClass().getSimpleName(),retVal);
    }

    @AfterReturning(pointcut = "execution(* com.lupur.api.controller.UserController.deleteUser(..)) && args(id)" ,returning = "retVal")
    public void afterReturningAdviceDeleteUser(JoinPoint jp, Object retVal, Integer id){
        Method method = ((MethodSignature) jp.getSignature()).getMethod();
        String path = REQUEST_MAPPING_PATH + method.getAnnotation(DeleteMapping.class).value()[0];
        Integer resourceId = ((UserDTO) retVal).getId();
        logger.info("Method: {}, Operation type: {} ,Path: {}, Parameters: {}, Resource id: {}, Returning: {}, Returned value: {}",jp.getSignature(),"DELETE",path,"Id: " + id,resourceId,retVal.getClass().getSimpleName(),retVal);
    }
}
