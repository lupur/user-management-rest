package com.lupur.api.exception.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.List;

/**
 * Created by Razvan Alexandru Lupu on 19-Jul-17.
 */

@ApiModel
public class ApiError {

    @ApiModelProperty(name = "HTTP status", value = "HTTP status", example = "404")
    private HttpStatus status;

    @ApiModelProperty(name = "Error message", value = "Error message", example = "No User found for ID 1")
    private String message;

    @ApiModelProperty(name = "Errors", value = "Errors")
    private List<String> errors;

    public ApiError(HttpStatus status, String message, List<String> errors) {
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public ApiError(HttpStatus status, String message, String error) {
        this.status = status;
        this.message = message;
        errors = Collections.singletonList(error);
    }

    public ApiError() {
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
