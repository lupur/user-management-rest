package com.lupur.api.exception.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Razvan Alexandru Lupu on 14-Jul-17.
 */
public class ValidationErrorDTO {

    @JsonProperty("errors")
    private List<FieldErrorDTO> fieldErrorDTOList;

    public ValidationErrorDTO(List<FieldErrorDTO> fieldErrorDTOList) {
        this.fieldErrorDTOList = fieldErrorDTOList;
    }

    public ValidationErrorDTO() {
        fieldErrorDTOList = new ArrayList<>();
    }

    public void addFieldErrorDTO(FieldErrorDTO fieldErrorDTO){
        fieldErrorDTOList.add(fieldErrorDTO);
    }

    public List<FieldErrorDTO> getFieldErrorDTOList() {
        return fieldErrorDTOList;
    }

    public void setFieldErrorDTOList(List<FieldErrorDTO> fieldErrorDTOList) {
        this.fieldErrorDTOList = fieldErrorDTOList;
    }
}
