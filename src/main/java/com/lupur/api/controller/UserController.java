package com.lupur.api.controller;

import com.lupur.api.exception.dto.ApiError;
import com.lupur.business.dto.UserDTO;
import com.lupur.business.service.UserService;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Razvan Alexandru Lupu on 13-Jul-17.
 */

@Api(value = "users", description = "This is the users controller")
@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get list of users",
            response = UserDTO.class,
            responseContainer = "List",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the list of users", responseHeaders = {@ResponseHeader(name = "content-type", description = "The content type of the response", response = String.class)}),
            @ApiResponse(code = 401, message = "You are not authorized to see this resource", response = ApiError.class),
            @ApiResponse(code = 403, message = "Accessing the resource you are trying to reach is forbidden", response = ApiError.class),
            @ApiResponse(code = 404, message = "The resource you are trying to reach is not found", response = ApiError.class)
    })
    public List<UserDTO> getUsers() {
        return userService.getUsers();
    }

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, params = "role")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find users by role",
            response = UserDTO.class,
            responseContainer = "List",
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the list of users", responseHeaders = {@ResponseHeader(name = "content-type", description = "The content type of the response", response = String.class)}),
            @ApiResponse(code = 401, message = "You are not authorized to see this resource", response = ApiError.class),
            @ApiResponse(code = 403, message = "Accessing the resource you are trying to reach is forbidden", response = ApiError.class),
            @ApiResponse(code = 404, message = "The resource you are trying to reach is not found", response = ApiError.class)
    })
    public List<UserDTO> getUsersByRole(@ApiParam(value = "role name", required = true) @RequestParam("role") String role) {
        return userService.findByRole(role);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Get user by id",
            response = UserDTO.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the user", responseHeaders = {@ResponseHeader(name = "content-type", description = "The content type of the response", response = String.class)}),
            @ApiResponse(code = 401, message = "You are not authorized to see this resource", response = ApiError.class),
            @ApiResponse(code = 403, message = "Accessing the resource you are trying to reach is forbidden", response = ApiError.class),
            @ApiResponse(code = 404, message = "The resource you are trying to reach is not found", response = ApiError.class)
    })
    @ApiParam(name = "user ID", required = true, type = "Integer")
    public UserDTO getUser(@ApiParam(value = "user ID", required = true) @Valid @PathVariable("id") Integer id) {
        return userService.getUser(id);
    }

    @PostMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation(value = "Create new user",
            response = UserDTO.class,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully created the user", responseHeaders = {@ResponseHeader(name = "content-type", description = "The content type of the response", response = String.class)}),
            @ApiResponse(code = 401, message = "You are not authorized to see this resource", response = ApiError.class),
            @ApiResponse(code = 403, message = "Accessing the resource you are trying to reach is forbidden", response = ApiError.class),
            @ApiResponse(code = 404, message = "The resource you are trying to reach is not found", response = ApiError.class)
    })
    public UserDTO createUser(@ApiParam(value = "user", required = true) @Valid @RequestBody UserDTO userDTO) {
        return userService.create(userDTO);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Delete user by id",
            response = UserDTO.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created the user", responseHeaders = {@ResponseHeader(name = "content-type", description = "The content type of the response", response = String.class)}),
            @ApiResponse(code = 401, message = "You are not authorized to see this resource", response = ApiError.class),
            @ApiResponse(code = 403, message = "Accessing the resource you are trying to reach is forbidden", response = ApiError.class),
            @ApiResponse(code = 404, message = "The resource you are trying to reach is not found", response = ApiError.class)
    })
    public UserDTO deleteUser(@ApiParam(value = "user ID", required = true) @PathVariable("id") Integer id) {
        return userService.deleteUser(id);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Update user",
            response = UserDTO.class,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully updated the user", responseHeaders = {@ResponseHeader(name = "content-type", description = "The content type of the response", response = String.class)}),
            @ApiResponse(code = 401, message = "You are not authorized to see this resource", response = ApiError.class),
            @ApiResponse(code = 403, message = "Accessing the resource you are trying to reach is forbidden", response = ApiError.class),
            @ApiResponse(code = 404, message = "The resource you are trying to reach is not found", response = ApiError.class)
    })
    public UserDTO updateUser(@ApiParam(value = "user ID", required = true) @PathVariable("id") Integer id,
                              @ApiParam(value = "user", required = true) @Valid @RequestBody UserDTO userDTO) {
        return userService.updateUser(id, userDTO);

    }


    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE, params = "username")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find user by username",
            response = UserDTO.class,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the user", responseHeaders = {@ResponseHeader(name = "content-type", description = "The content type of the response", response = String.class)}),
            @ApiResponse(code = 401, message = "You are not authorized to see this resource", response = ApiError.class),
            @ApiResponse(code = 403, message = "Accessing the resource you are trying to reach is forbidden", response = ApiError.class),
            @ApiResponse(code = 404, message = "The resource you are trying to reach is not found", response = ApiError.class)
    })
    public UserDTO findByUsername(@ApiParam(value = "username", required = true, type = "String") @RequestParam("username") String username) {
        return userService.findByUsername(username);
    }


}
