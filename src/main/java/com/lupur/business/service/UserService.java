package com.lupur.business.service;

import com.lupur.business.dto.UserDTO;
import com.lupur.business.mapper.UserMapper;
import com.lupur.data.entity.User;
import com.lupur.data.repository.UserJdbcTemplate;
import com.lupur.data.repository.UserJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Razvan Alexandru Lupu on 13-Jul-17.
 */

@Service
public class UserService {

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private UserJdbcTemplate userJdbcTemplate;

    @Autowired
    private UserMapper userMapper;


    @Transactional
    public UserDTO create(UserDTO userDTO) {
        User user = userMapper.getEntity(userDTO);
        User newUser = userJpaRepository.save(user);
        return userMapper.getDTO(newUser);

    }

    @Transactional(readOnly = true)
    public List<UserDTO> getUsers() {
        return userJpaRepository.findAll().stream().map(userMapper::getDTO).collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    public UserDTO getUser(Integer id) {
        User user = userJpaRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("No User found for ID " + id);
        }
        return userMapper.getDTO(user);
    }

    @Transactional(readOnly = true)
    public UserDTO findByUsername(String username) {
        User user = userJpaRepository.findByUsername(username);
        if (user == null) {
            throw new EntityNotFoundException("No User found for username " + username);
        }
        return userMapper.getDTO(user);
    }

    @Transactional(readOnly = true)
    public List<UserDTO> findByRole(String roleName) {
        return userJdbcTemplate.findByRole(roleName).stream().map(userMapper::getDTO).collect(Collectors.toList());
    }

    public UserDTO deleteUser(Integer id) {
        User user = userJpaRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("No User found for ID " + id);
        }
        userJpaRepository.delete(id);
        return userMapper.getDTO(user);
    }

    @Transactional
    public UserDTO updateUser(Integer id, UserDTO userDTO) {
        User user = userJpaRepository.findOne(id);
        if (user == null) {
            throw new EntityNotFoundException("No User found for ID " + id);
        }
        userDTO.setId(id);
        User updatedUser = userJpaRepository.save(userMapper.getEntity(userDTO));
        return userMapper.getDTO(updatedUser);
    }
}

