package com.lupur.business.mapper;

import com.lupur.business.dto.UserDTO;
import com.lupur.data.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Razvan Alexandru Lupu on 13-Jul-17.
 */

@Component
public class UserMapper {



    @Autowired
    private ModelMapper modelMapper;

    public UserDTO getDTO(User user){
        return modelMapper.map(user,UserDTO.class);
    }

    public User getEntity(UserDTO userDTO){
        return modelMapper.map(userDTO,User.class);
    }
}

