package com.lupur.business.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

/**
 * Created by Razvan Alexandru Lupu on 13-Jul-17.
 */

@ApiModel("User")
public class UserDTO {

    private static final String ONLY_LETTERS_PATTERN = "^[A-Za-z]*$";

    @ApiModelProperty(name = "Id", notes = "The user id", example = "1")
    private Integer id;

    @ApiModelProperty(name = "First name", notes = "The first name", required = true, example = "John")
    @NotNull
    @Size(max = 45, min = 3)
    @Pattern(regexp = ONLY_LETTERS_PATTERN)
    private String firstName;

    @ApiModelProperty(name = "Last name", notes = "The last name", required = true, example = "Branch")
    @NotNull
    @Size(max = 45, min = 3)
    @Pattern(regexp = ONLY_LETTERS_PATTERN)
    private String lastName;

    @ApiModelProperty(name = "Username", notes = "The username", required = true, example = "johnbranch")
    @NotNull
    @Size(max = 45, min = 3)
    private String username;

    @ApiModelProperty(name = "Password", notes = "The password", required = true, example = "comp!3x-Pa$$word")
    @NotNull
    @Size(max = 60, min = 8)
    private String password;

    @ApiModelProperty(name = "Email", notes = "The email", required = true, example = "john.branch@example.com")
    @NotNull
    private String email;

    @ApiModelProperty(name = "Date of birth", notes = "The date of birth", required = true, example = "1995-07-15")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @NotNull
    @Past
    private Date dateOfBirth;

    @ApiModelProperty(name = "Phone", notes = "The phone number", required = true, example = "0728098896")
    @NotNull
    @Size(max = 45, min = 6)
    @NumberFormat(style = NumberFormat.Style.NUMBER)
    private String phone;

    @ApiModelProperty(name = "Address", notes = "The address", required = true, example = "Cluj-Napoca")
    @NotNull
    @Size(max = 45)
    private String address;

    @ApiModelProperty(name = "first name", notes = "The first name", allowEmptyValue = true, allowableValues = "{ \"id\": 1, \"roleName\": \"ADMIN\" },{ \"id\": 2, \"roleName\": \"USER\" }")
    private Set<RoleDTO> roles;

    public UserDTO(Integer id, String firstName, String lastName, String username, String password, String email, Date dateOfBirth, String phone, String address, Set<RoleDTO> roles) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.password = password;
        this.email = email;
        this.dateOfBirth = dateOfBirth;
        this.phone = phone;
        this.address = address;
        this.roles = roles;
    }

    public UserDTO() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Set<RoleDTO> getRoles() {
        return roles;
    }

    public void setRoles(Set<RoleDTO> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("{");
        stringBuilder.append("ID: ").append(id).append(", ");
        stringBuilder.append("First name: ").append(firstName).append(", ");
        stringBuilder.append("Last name: ").append(lastName).append(", ");
        stringBuilder.append("Username: ").append(username).append(", ");
        stringBuilder.append("Password: ").append("[PROTECTED]").append(", ");
        stringBuilder.append("Email: ").append(email).append(", ");
        stringBuilder.append("Date of birth: ").append(dateOfBirth).append(", ");
        stringBuilder.append("Phone: ").append(phone).append(", ");
        stringBuilder.append("Address: ").append(address).append(", ");
        stringBuilder.append("Roles: ").append(roles);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(this.id)
                .append(this.firstName)
                .append(this.lastName)
                .append(this.username)
                .append(this.password)
                .append(this.email)
                .append(this.dateOfBirth)
                .append(this.phone)
                .append(this.address)
                .append(this.roles)
                .toHashCode();

    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (obj == this) return true;
        if (!(obj instanceof UserDTO)) return false;
        UserDTO userDTO = (UserDTO) obj;
        return new EqualsBuilder()
                .append(this.id, userDTO.getId())
                .append(this.firstName, userDTO.getFirstName())
                .append(this.lastName, userDTO.getLastName())
                .append(this.username, userDTO.getUsername())
                .append(this.password, userDTO.getPassword())
                .append(this.email, userDTO.getEmail())
                .append(this.dateOfBirth, userDTO.getDateOfBirth())
                .append(this.phone, userDTO.getPhone())
                .append(this.address, userDTO.getAddress())
                .append(this.roles, userDTO.getRoles())
                .isEquals();
    }
}
